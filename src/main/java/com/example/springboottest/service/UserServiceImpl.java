package com.example.springboottest.service;

import com.example.springboottest.entity.User;
import com.example.springboottest.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo repo;

    @Override
    public void add(User user) {
        repo.save(user);
    }

    @Override
    public List<User> getAll() {
        return repo.findAll();
    }
}
