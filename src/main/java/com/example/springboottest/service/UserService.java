package com.example.springboottest.service;

import com.example.springboottest.entity.User;

import java.util.List;


public interface UserService {
    void add(User user);
    List<User> getAll();
}
