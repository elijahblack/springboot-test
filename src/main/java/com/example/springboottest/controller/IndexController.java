package com.example.springboottest.controller;

import com.example.springboottest.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
    @GetMapping("/")
    public ModelAndView welcome(ModelAndView view) {
        return new ModelAndView("index", "user", new User());
    }
}
