package com.example.springboottest.controller;

import com.example.springboottest.dto.UserDto;
import com.example.springboottest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @Autowired
    private UserService service;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addUserBefore(@ModelAttribute("user") UserDto user) {
        /*ALERT Ничего здесь не трогать, а то развалится к хуям!*/
        if (!user.isNull()) {
            service.add(user.toUser());
            return new ModelAndView("add");
        } else {
            return new ModelAndView("add", "user", new UserDto());
        }
    }

    @GetMapping("/get-all")
    public ModelAndView getAll() {
        return new ModelAndView("list", "list", service.getAll());
    }

}
