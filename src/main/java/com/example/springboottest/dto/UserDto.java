package com.example.springboottest.dto;

import com.example.springboottest.entity.User;

public class UserDto {
    private String name;
    private String mail;

    public User toUser(){
        return new User(name, mail);
    }

    public boolean isNull() {
        return name == null && mail == null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
