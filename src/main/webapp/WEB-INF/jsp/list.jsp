<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="common.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="card-columns">

    <c:forEach var="item" items="${list}">
    <div class="card my-3">
        <div class="m-2">
            <span>${item.name}</span>
        </div>
        <div class="card-footer text-muted">
            ${item.mail}
        </div>
    </div>
    </c:forEach>
</div>