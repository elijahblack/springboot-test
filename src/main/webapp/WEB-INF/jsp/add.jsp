<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@include file="common.jsp" %>

<form class="container">
    <div id="add">
        <%--@elvariable id="user" type="com.example.springboottest.dto.UserDto"--%>
        <form:form action="/add" method="post"  
                   modelAttribute="user">
            <div class="form-group">
                <label>Enter Name</label>
                <form:input path="name" class="form-control w-25"/>
            </div>
            <div class="form-group">
                <label>Enter Mail</label>
                <form:input path="mail" class="form-control w-25"/>
            </div>
            <input type="submit" class="btn btn-primary"/>
        </form:form>
    </div>
</form>